{{ if exists items }}
{{ calendar:prelink }}
<table class="contentpaneopen" border="0">
    <tbody>
        <tr class="headingrow">
            <td width="100%" class="contentheading">
                <h2 class="contentheading">
                    {{ date }}
                </h2>
            </td>
        </tr>
        <?php foreach ($items as $item): ?>
        <tr>
            <td class="ev_detail" align="left" valign="top" colspan="4">
                <b>Activiteit: </b> <a href="<?php echo $prelink?>/<?php echo $item->id  ?>" ><?php echo  $item->name ?></a><br>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
{{ else }}
<p>
    Vandaag geen activiteiten.
</p>
{{ endif }}