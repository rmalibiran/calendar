<section class="title">
	<h4>
		<?php echo lang('calendar:'.$this->method); ?>
	</h4>
</section>
<section class="item">
	<div class="content">
		<?php echo form_open(); ?>
		<div class="tabs">
			<div class="form_inputs" id="page-layout-html">
				<fieldset>
					<ul>
						<li class="even">
							<label for="title"><?php echo lang('global:title');?> <span>*</span></label>
							<div class="input">
								<?php echo form_input('name', $name, 'maxlength="60"'); ?>
							</div>
						</li>
						<li class="uneven">
							<label for="description"><?php echo lang('calendar:description'); ?></label> <?php echo form_textarea(array('name'=>'description', 'value' => $description, 'rows' => 5)); ?>
						</li>
						<li class="even">
							<label for="color"><?php echo lang('calendar:color'); ?></label>
							<div class="input">
								<?php echo form_input(array('type'=>'color', 'id'=>'colorpickerHolder','name'=>'color','value'=>$color)) ?>
							</div>
						</li>
					</ul>
				</fieldset>
			</div>
		</div>
		<div class="buttons float-right padding-top">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
		</div><?php echo form_close(); ?>
	</div>
</section>