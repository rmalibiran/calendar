<section class="title">
	<h4>
		<?php echo lang('calendar:'.$this->method); ?>
	</h4>
</section>
<section class="item">
	<div class="content">
		<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="form_inputs">
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="name"><?php echo lang('calendar:name'); ?><span>*</span></label>
					<div class="input">
						<?php echo form_input('name', set_value('name', $name), 'class="width-15"'); ?>
					</div>
				</li>
				<li class="date-meta">
					<label><?php echo lang('calendar:starttime'); ?><span>*</span></label>
					<div class="input datetime_input">
						<?php echo form_input('start_on', date('Y-m-d', $rightstarttime), 'maxlength="10" id="datepicker" class="text width-20"'); ?>&nbsp; <?php echo form_dropdown('start_on_hour', $hours, date('H', $rightstarttime)) ?> : <?php echo form_dropdown('start_on_minute', $minutes, date('i', $rightstarttime)); ?>
					</div>
				</li>
				<li class="date-meta">
					<label><?php echo lang('calendar:stoptime'); ?><span>*</span></label>
					<div class="input datetime_input">
						<?php echo form_input('stop_on', date('Y-m-d', $rightstoptime), 'maxlength="10" id="datepicker2" class="text width-20"'); ?>&nbsp; <?php echo form_dropdown('stop_on_hour', $hours, date('H', $rightstoptime)) ?> : <?php echo form_dropdown('stop_on_minute', $minutes, date('i', $rightstoptime)); ?>
					</div>
				</li>
				<li>
					<label for="published"><?php echo lang('calendar:published');?></label>
					<div class="input">
						<?php echo form_checkbox('published', 1, $published, 'class="width-15"'); ?>
					</div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="description"><?php echo lang('calendar:description'); ?></label> <?php echo form_textarea(array('id' => 'description', 'name' => 'description', 'value' => $description, 'rows' => 30, 'class' => 'wysiwyg-simple')); ?>
				</li>
				<li class="<?php echo alternator('', 'uneven'); ?>">
					<label for="location"><?php echo lang('calendar:location'); ?> <span>*</span></label>
					<div class="input">
						<?php echo form_input('location', set_value('location', $location), 'class="width-15"'); ?>
					</div>
				</li>
				<li class="<?php echo alternator('', 'uneven'); ?>">
					<label for="price"><?php echo lang('calendar:price'); ?> <span>*</span></label>
					<div class="input">
						<?php echo form_input('price', set_value('price', $price), 'class="width-15"'); ?>
					</div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label><?php echo lang('calendar:categorie'); ?><span>*</span></label>
					<div class="input">
						<?php echo form_dropdown('categorie_id', $categories, $categorie_id) ?>
					</div>
				</li>
			</ul>
		</div>
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div><?php echo form_close(); ?>
	</div>
</section>